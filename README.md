Tasks to answer in your own README.md that you submit on Canvas:
BEN DAVIS 
1.  See logger.log, why is it different from the log to console?
	a. Logger.log contained this:
2019-03-25 14:43:08 INFO edu.baylor.ecs.si.Timer <clinit> starting the app
2019-03-25 14:43:08 INFO edu.baylor.ecs.si.Timer timeMe Calling took: 1
2019-03-25 14:43:08 INFO edu.baylor.ecs.si.Timer timeMe * should take: 0
2019-03-25 14:43:08 FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
2019-03-25 14:43:08 FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
2019-03-25 14:43:09 INFO edu.baylor.ecs.si.Timer timeMe Calling took: 1001
2019-03-25 14:43:09 INFO edu.baylor.ecs.si.Timer timeMe * should take: 1000

Console contained this:
2019-03-25 14:43:08 INFO edu.baylor.ecs.si.Timer <clinit> starting the app
2019-03-25 14:43:08 INFO edu.baylor.ecs.si.Timer timeMe Calling took: 1
2019-03-25 14:43:08 INFO edu.baylor.ecs.si.Timer timeMe * should take: 0
2019-03-25 14:43:09 INFO edu.baylor.ecs.si.Timer timeMe Calling took: 1001
2019-03-25 14:43:09 INFO edu.baylor.ecs.si.Timer timeMe * should take: 1000

The logger file contained FINER while console did not because the logger for the log file was set to a different logging level.
File logging level is set to ALL while Console logging is set to INFO level


1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
	a. this comes from an message created by a junit class named ConditionEvaluation which is a FINER level message logged by the logger while running the Tester class.
	Comes from logger which from the logger in junit. 
1.  What does Assertions.assertThrows do?
	This allows you to test multiple exceptions within the same test. 
	It asserts that execution of the supplied executable throws an exception of the expectedType and
	returns the exception. If no exception is thrown, or an exception of a different type is thrown
	this method will fail. 
1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
		a. The serialization runtime associates with each serializable class a version number called
		a serialVersionUID which is used during deserialization to verify that the sender and reciever of a serialized object 
		have loaded classes for that object are compatible with respect to serialization. If the reciever class has loaded a class for the 
		object that has a dfferent serialVersionUID than that of the corresponding sender's class than deserialization will result in an InvalidClassException.
		A serializable class can declare its own serialVersionUID explicitly, or it will be automatically geerated. 
    2.  Why do we need to override constructors?
		a. constructors are not inherited. everytime you inherit from class you have to over ride constructor to be able to call the super's constructor
    3.  Why we did not override other Exception methods?
		a. we wanted to use the other exception methods provided from extending Exception.
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
	a. static blocks are initialization blocks which get called once at the start of your program.
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
	README.md is of md file extension or Markdown language. The file extension you use with your README determines 
		how Bitbucket parses the markup in your file. 
		For example, Bitbucket interprets the README.md as a file that uses the Markdown language.
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
	a. the test is failing because the tests are taking too 1 second too long to run. 
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
	a. the test is failing because timeNow is set to null but the try catch block catches an exception before timeNow can be set.
	when system.current time tries to subtract timeNow, timeNow is null.
	The sequences is as follows: Assertions.assertThrows(TimerException()->Timer.timeMe(-1);
	In timeMe, timeNow is set to null and if the time passed is < 0 a new Timer Exception is thrown. timeNow cannot be 
	initialized so in the finally block there is nothing for system.currenttimeMillis to subtract from.
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
	
1.  Make a printScreen of your eclipse Maven test run, with console

1.  What category of Exceptions is TimerException and what is NullPointerException
	Timer exception is a checked exception. The compiler forced the programmer 
		to add throws to the funciton declaration of timeMe and in the tester method.
	NullPointerException is an unchecked exception
1.  Push the updated/fixed source code to your own repository.
https://bitbucket.org/Ben_Davis3/bdavisexceptionrunnerlab/src/master/